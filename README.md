# Personal prometheus gitlab exporter

Forked from https://github.com/nomaster/gitlab-exporter

This uses `Gitlab <https://gitlab.com>`'s API to fetch some data and exposes
it to `Prometheus <https://prometheus.io/>`\_\_.

## Getting Started

Get your personal access token from
`https://git.example.com/profile/personal_access_tokens`

### Run with Docker

    docker run --rm -it \
    -p 3001:3001 \
    -e URL=$YOUR_GITLAB_URL
    -e TOKEN=$YOUR_GITLAB_TOKEN
    registry.0xacab.org/varac/personal-gitlab-exporter:latest

### Run from Git

Install requirements, e.g.:

    pip install --user -r requirements.txt

Run this

    ./gitlab_exporter.py

## Check metrics

    curl localhost:3001/metrics

## Config

The following environment variables may be set::

    LOGLEVEL=INFO PORT=3002 INTERVAL=180 ./gitlab_exporter.py

==================== ===========
Environment Variable Description
==================== ===========
LOGLEVEL Standard Python `log level`\_
PORT Port to listen on
INTERVAL Interval in seconds to wait between data fetches
URL URL to your GitLab installation
TOKEN Your personal access token for the GitLab API
==================== ===========

log level: https://docs.python.org/3.5/library/logging.html#levels

## Gitlab API

- REST API Reference https://github.com/gitlabhq/gitlabhq/tree/master/doc/api
- Python API http://python-gitlab.readthedocs.io/en/stable/api-usage.html

## Prometheus Exporter

https://github.com/prometheus/client_python
