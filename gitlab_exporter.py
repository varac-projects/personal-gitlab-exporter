#!/usr/bin/env python3
# encoding: utf-8
"""Prometheus exporter for personal gitlab metrics.

Based on https://python-gitlab.readthedocs.io/en/stable/
"""

import logging
import os
import time
import re
import gitlab
import dateutil.parser
from prometheus_client import start_http_server, Gauge


def get_projects(gitl, log, group=None, regex_str=''):
    """Return a list of project."""
    try:
        if group:
            all_projects = group.projects.list(
                get_all=True, archived=False)
        else:
            all_projects = gitl.projects.list(
                get_all=True, archived=False)
        # log.debug(f"All projects: {all_projects}\n")
        log.debug(f"Total Project Count: {len(all_projects)}")

        # Filter projects by regex_str
        regex = re.compile(regex_str)
        projects = [p for p in all_projects if regex.match(p.name)]
        # log.debug(f"Filtered projects: {projects}\n")
        log.debug(f"Filtered Project Count: {len(projects)}")
        return projects
    except gitlab.exceptions.GitlabListError:
        log.warning("Projects could not be retrieved\n")
        return []


def get_pipeline(log, project, pipeline_id: int):
    """Return details of pipeline for a given id."""
    pipeline = None
    try:
        pipeline = project.pipelines.get(pipeline_id)
        # log.debug(f"Pipeline: {pipeline}\n")
        return pipeline
    except gitlab.exceptions.GitlabListError:
        log.warning("Pipeline could not be retrieved")
        log.debug(f"Pipeline: {pipeline}")
        return None


def get_pipelines(log, project, max_pipelines=20):
    """Return a list of pipelines for a given project."""
    if project.builds_access_level != 'disabled':
        try:
            pipelines = project.pipelines.list(
                get_all=False, ref=project.default_branch, per_page=max_pipelines)
            log.debug(f"Pipelines: {pipelines}")
            return pipelines
        except gitlab.exceptions.GitlabListError:
            log.warning("Pipelines could not be retrieved")
            log.debug(f"Pipelines: {pipelines}")
            return []
    else:
        return []


def get_default_pipeline_success(log, project):
    """Check if last default pipelines for a given project was successful."""
    if project.jobs_enabled:
        try:
            pipeline = project.pipelines.list(ref=project.default_branch, scope='finished',
                                              get_all=False, per_page=1)[0]
            log.debug(f"Last default Pipeline id: {pipeline.id}")
            # log.debug(f"Last default Pipeline: {pipeline}")
            return pipeline.status != 'failed'
        except gitlab.exceptions.GitlabListError:
            log.warning("Pipelines could not be retrieved")
            return None
        except IndexError:
            log.warning("Pipelines could not be retrieved")
            return None
    else:
        return None


def get_last_default_pipeline_for_status(log, project, status):
    """Return last pipeline for a given project matching given status."""
    if project.jobs_enabled:
        try:
            pipeline = project.pipelines.list(ref=project.default_branch, scope='finished',
                                              get_all=False, per_page=1, status=status)[0]
            pipeline_details = get_pipeline(log, project, pipeline.id)
            # log.debug(f"Last default pipeline id with status \"{status}\": {pipeline_details}\n")
            return pipeline_details
        except gitlab.exceptions.GitlabListError:
            log.warning("Pipeline could not be retrieved")
            return None
        except IndexError:
            log.warning("Pipeline could not be retrieved")
            return None
    else:
        return None


def get_merge_reqs(project, state='opened'):
    """Return a list of all project merge reqs."""
    if project.merge_requests_enabled:
        merge_reqs = project.mergerequests.list(
            state=state, get_all=False, per_page=20)
    else:
        merge_reqs = []
    return merge_reqs


def get_stats(gitl, log, group, regex, projects_total,
              pipelines_status_count, pipeline_failure_rate,
              open_unassigned_merge_requests, open_issues_total,
              pipeline_default_failed,
              pipeline_default_last_success, pipeline_default_last_failed,
              pipeline_duration_seconds,
              max_pipelines):
    """Get all stats."""
    projects = get_projects(gitl, log, group, regex)
    projects_total.set(len(projects))
    log.info('Total filtered projects: ' + str(len(projects)))
    for project in projects:
        project = gitl.projects.get(project.id)
        log.info('')
        log.info(f"Project: {project.name} ({project.id})")
        # log.debug(project)

        # Get last pipelines and count success and failures
        pipelines = get_pipelines(log, project, max_pipelines)
        if pipelines:
            success_pipelines = [p for p in pipelines if p.status == 'success']
            failed_pipelines = [p for p in pipelines if p.status == 'failed']
            etc_pipelines = [
                p for p in pipelines if p.status not in ['failed', 'success']]

            pipelines_status_count.labels(project_id=project.id, project_name=project.name,
                                          status='success').set(len(success_pipelines))
            pipelines_status_count.labels(project_id=project.id, project_name=project.name,
                                          status='failed').set(len(failed_pipelines))
            pipelines_status_count.labels(project_id=project.id, project_name=project.name,
                                          status='etc').set(len(etc_pipelines))

            success_and_failed_pipelines = failed_pipelines + success_pipelines
            if len(success_and_failed_pipelines) > 0:
                # Calculate pipeline failure rate
                pipeline_failure_rate.labels(project_id=project.id, project_name=project.name
                                             ).set(len(failed_pipelines) * 100 /
                                                   len(failed_pipelines + success_pipelines))

        # for i in etc_pipelines:
        #     log.debug("{}: {}".format(i.id, i.status))

        # Open, unassigned merge reqs
        mrs = get_merge_reqs(project)
        open_unassigned_merge_requests.labels(
            project_id=project.id,
            project_name=project.name).set(
                len(mrs))

        # Open issues
        open_issues_total.labels(
            project_id=project.id,
            project_name=project.name).set(
                project.open_issues_count)

        # Check if last default pipeline was successful
        default_pipeline_success = get_default_pipeline_success(log, project)
        log.info(f'default pipeline success: {default_pipeline_success}')
        if default_pipeline_success is not None:
            log.info(
                f'Saving metric for default pipeline success: {default_pipeline_success}')
            pipeline_default_failed.labels(
                project_id=project.id,
                project_name=project.name).set(not default_pipeline_success)

        # last successful default pipeline
        pipeline = get_last_default_pipeline_for_status(
            log, project, "success")
        if pipeline is not None:
            log.info(
                f'Last successful default pipeline: {pipeline.id} ({pipeline.updated_at})')
            log.info('Saving metric for last successful default pipeline:'
                     f'{pipeline.id} ({pipeline.updated_at})')
            started = dateutil.parser.parse(pipeline.started_at)
            finished = dateutil.parser.parse(pipeline.finished_at)
            finished_seconds = finished.strftime('%s')
            pipeline_default_last_success.labels(
                project_id=project.id,
                project_name=project.name).set(finished_seconds)

            # Calculate duration
            # The API provides a `duration` metric, however this is misleading
            # because it only shows the parent pipeline duration, without the
            # child pipelines. That's why we need to calculate.

            duration = round(finished.timestamp() - started.timestamp())
            log.debug(f'Pipeline {pipeline.id} started: {pipeline.started_at}, finished: '
                      f'{pipeline.finished_at}, duration: {duration}')
            pipeline_duration_seconds.labels(
                project_id=project.id,
                project_name=project.name,
                status=pipeline.status,
                ref=pipeline.ref).set(duration)

        # last failed default pipeline
        pipeline = \
            get_last_default_pipeline_for_status(log, project, "failed")
        if pipeline is not None:
            log.info(f'Saving metric for last failed default pipeline: {pipeline.id} '
                     f'({pipeline.updated_at})')
            parsed_date = dateutil.parser.parse(pipeline.updated_at)
            parsed_date_seconds = parsed_date.strftime('%s')
            pipeline_default_last_failed.labels(
                project_id=project.id,
                project_name=project.name).set(parsed_date_seconds)


def main():
    # pylint: disable-msg=bare-except
    """Main function."""
    loglevel = os.environ.get('LOGLEVEL', 'WARN').upper()

    log = logging.getLogger(__name__)
    # https://stackoverflow.com/a/20112491
    log_format = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
    logging.basicConfig(format=log_format)
    log.setLevel(level=loglevel)

    # time to sleep between calls to gitlab
    interval = int(os.environ.get('INTERVAL', 300))

    # port to listen on
    port = int(os.environ.get('PORT', 3001))

    # URL of the GitLab instance, defaults to hosted GitLab
    url = str(os.environ.get('URL', 'https://gitlab.com'))

    # require secret token for the app to authenticate itself
    token = str(os.environ['TOKEN'])
    assert token != ''

    max_pipelines = int(os.environ.get('MAX_PIPELINES', 20))

    # for group queries, i.e. 'openappstack'
    group = str(os.environ.get('GROUP', None))

    # optionally limit projects by a regex string
    # i.e. '^bootstrap$'
    project_regex = str(os.environ.get('PROJECT_REGEX', ''))

    log.debug('INTERVAL:       %s', str(interval))
    log.debug('PORT:           %s', str(port))
    log.debug('URL:            %s', url)
    # log.debug('TOKEN:          %s', token)
    log.debug('MAX_PIPELINES:  %s', max_pipelines)
    log.debug('GROUP:          %s', group)
    log.debug('PROJECT_REGEX: %s', project_regex)

    # Initialize Prometheus instrumentation
    projects_total = Gauge('gitlab_projects_total', 'Number of projects')
    pipelines_status_count = Gauge(
        'gitlab_pipelines_status_count', f'Last {max_pipelines} pipelines count per status', [
            'project_id', 'project_name', 'status'])
    pipeline_failure_rate = Gauge(
        'gitlab_pipeline_failure_rate',
        f'Failure rate for the last {max_pipelines} pipelines in %',
        ['project_id', 'project_name'])
    open_issues_total = Gauge(
        'gitlab_open_issues_total', 'Number of open issues', [
            'project_id', 'project_name'])
    pipeline_duration_seconds = Gauge(
        'gitlab_pipeline_duration_seconds', 'Seconds the pipeline took to run',
        ['project_id', 'project_name', 'status', 'ref'])
    pipeline_default_failed = Gauge(
        'gitlab_pipeline_default_failed',
        'Did the last default pipeline fail? (0 for false or 1 for true)', [
            'project_id', 'project_name'])
    pipeline_default_last_success = Gauge(
        'gitlab_pipeline_default_last_success',
        'Time of last successful default pipeline', [
            'project_id', 'project_name'])
    pipeline_default_last_failed = Gauge(
        'gitlab_pipeline_default_last_failed',
        'Time of last failed default pipeline', [
            'project_id', 'project_name'])
    open_unassigned_merge_requests = Gauge(
        'gitlab_open_unassigned_merge_requests',
        'Number of open, unassigned merge requests ', [
            'project_id', 'project_name'])

    # Start prometheus exporter endpoint
    start_http_server(port)
    log.setLevel(loglevel)
    log.info('listening on port %s\n', format(port))

    # Login to GitLab
    gitl = gitlab.Gitlab(url, token)

    gitl_group = None
    if group:
        while True:
            try:
                gitl_group = gitl.groups.get(group)
                break
            except KeyboardInterrupt:
                break
            except:
                log.info('Cant get group - sleeping for 10 seconds')
                time.sleep(10)

    while True:
        try:
            get_stats(gitl, log, gitl_group, project_regex, projects_total,
                      pipelines_status_count, pipeline_failure_rate,
                      open_unassigned_merge_requests, open_issues_total,
                      pipeline_default_failed,
                      pipeline_default_last_success,
                      pipeline_default_last_failed, pipeline_duration_seconds,
                      max_pipelines)
            log.info('sleeping for %s seconds.\n', format(interval))
            time.sleep(interval)
        except KeyboardInterrupt:
            break


if __name__ == '__main__':
    main()
